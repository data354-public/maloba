# maloba

African languages meet AI

This repo contains contains our first attempt to create a lingala translator.

## Origin

The **maloba** means "paroles" in Lingala (other options where  **Balobi** => parler, ...)

## Ressources

**Dictionnaries**
* [glosbe](https://glosbe.com/ln/fr/...%20t%C9%9B%CC%81): Un projet de plusieurs langues. De nombreux textes avec leurs traductions
* [ikuska](http://www.ikuska.com/Africa/Lenguas/lingala/leccion10.htm): 
* [glosbe](http://www.suka-epoque.de/Suka-Epoque.htm): Quelques petites histoires et un petit dico
* [grouselle](http://pascal.grouselle.pagesperso-orange.fr/linfrad.htm): pas grand chose
* [lingala.be](http://dic.lingala.be/): Dico plus exemples
* [memrise](https://www.memrise.com/course/81398/phrases-en-lingala-1/): qq phrases et traductions
* [ksludotique](https://www.ksludotique.com/espace-lingala/lingala-expressions-courantes/): Quelques petites histoires et un petit dico
* [aggelia](http://web.archive.org/web/20160303193945/http://lingala.aggelia.be/liste_lingala.html): quelques phrases
